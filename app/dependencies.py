from .database.engine import SessionLocal
from fastapi.security import OAuth2PasswordBearer
from fastapi import HTTPException, Depends, Request, WebSocket
from jose import JWTError, jwt
from typing import Annotated


class CustomOAuth2PasswordBearer(OAuth2PasswordBearer):
    async def __call__(self, request: Request = None, websocket: WebSocket = None):
        return await super().__call__(request or websocket)


oauth2_scheme = CustomOAuth2PasswordBearer(tokenUrl="/users/login")

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"


def db() -> SessionLocal:
    db_connection = SessionLocal()
    try:
        yield db_connection
    finally:
        db_connection.close()


def create_access_token(user_id: int) -> str:
    encoded_jwt = jwt.encode({"user_id": user_id}, SECRET_KEY, ALGORITHM)
    return encoded_jwt


async def verify_token_access(token: Annotated[str, Depends(oauth2_scheme)]):
    try:
        data = jwt.decode(token, SECRET_KEY, algorithms=ALGORITHM)
        return data
    except JWTError:
        raise HTTPException(status_code=401)
