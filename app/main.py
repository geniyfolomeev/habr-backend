import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import (
    CORSMiddleware,
)
from app.routers import users
from app.routers import countries
from app.routers import articles
from app.routers import friends
from app.routers import message


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(users.router)
app.include_router(countries.router)
app.include_router(articles.router)
app.include_router(friends.router)
app.include_router(message.router)

if __name__ == '__main__':
    uvicorn.run("main:app", reload=True)
