import sqlalchemy.exc
from sqlalchemy.orm import Session, joinedload
from sqlalchemy.sql.expression import false, and_, true, or_
from sqlalchemy import case
from . import models
from app.routers.models import (
    UserCreate,
    UserUpdate,
    ArticleCreate,
    ArticleUpdate,
)


class NoPermissionError(Exception):
    pass


class DbError(Exception):
    pass


class LoginIsNotUniq(Exception):
    pass


class NotFoundInDB(Exception):
    pass


class AlreadyExists(Exception):
    pass


def get_users_filter(db: Session, user_id: int, limit: int, login: str, name: str, lastname: str, offset: int):
    query = db.query(
        models.User,
        case(
            # Если текущий пользак - чей то друг и не pending - Друг
            (and_(models.Friend.friend_id == user_id, models.Friend.pending == false()), 'FRIEND'),
            # Если текущий пользак - автор заявки и pending - Отправлена заявка
            (and_(models.Friend.user_id == 1, models.Friend.pending == true()), 'YOU_REQUESTED_FRIENDSHIP'),
            # Если текущий пользак - чей то друг и pending - Получена заявка
            (and_(models.Friend.friend_id == 1, models.Friend.pending == true()), 'REQUESTED_FRIENDSHIP_WITH_YOU'),
            else_='NOT_FRIEND'
        ).label('is_friend')
    ).outerjoin(
        models.Friend, or_(models.Friend.user_id == models.User.id, models.Friend.friend_id == models.User.id)
    ).filter(
        models.User.id != user_id,
    ).distinct(
        models.User.id,
    )

    if login:
        query = query.where(
            models.User.login.like(f'%{login}%')
        )
    if name:
        query = query.where(
            models.User.name.like(f'%{name}%')
        )
    if lastname:
        query = query.where(
            models.User.lastname.like(f'%{lastname}%')
        )

    users = []
    for user, status in query.offset(offset).limit(limit):
        user_model = user.__dict__
        user_model["is_friend"] = status
        users.append(user_model)

    return users


def get_user_by_login(db: Session, login: str):
    return db.query(models.User).filter_by(login=login).first()


def get_user_by_id(db: Session, user_id: int):
    db_user = db.query(models.User).filter_by(id=user_id)
    if db_user.count() == 0:
        raise NotFoundInDB(f"Пользователя с id = {user_id} не существует")
    return db_user.first()


def delete_user(db: Session, user_id: int, current_user_id: int) -> None:
    db_user = db.query(models.User).filter_by(id=user_id)

    if db_user.count() == 0:
        raise NotFoundInDB(f"Пользователя с id = {user_id} не существует")

    if db_user.first().id != current_user_id:
        if current_user_id != 1:
            raise NoPermissionError(f"У пользователя с id = {current_user_id} нет прав на эту операцию")

    friends = db.query(models.Friend).filter(
        ((models.Friend.user_id == user_id) | (models.Friend.friend_id == user_id)),
    )

    if friends.all():
        friends.delete()

    db_user.delete()
    db.commit()


def update_user(db: Session, user: UserUpdate, current_user_id: int):
    db_user = db.query(models.User).filter_by(id=user.id)
    if db_user.count() == 0:
        raise NotFoundInDB(f"User with id = {user.id} not found")

    if db_user.first().id != current_user_id:
        if current_user_id != 1:
            raise NoPermissionError(f"У пользователя с id = {current_user_id} нет прав на эту операцию")

    if user.login:
        user_same_login = db.query(models.User).filter_by(login=user.login).first()
        if user_same_login and user_same_login.id != user.id:
            raise LoginIsNotUniq(f"Логин = {user.login} занят другим пользователем")

    try:
        db_user.update(user.model_dump(exclude_unset=True, exclude_none=True))
        db.commit()
        return db.query(models.User).filter_by(id=user.id).first()
    except sqlalchemy.exc.IntegrityError as e:
        raise DbError(str(e))


def create_new_user(db: Session, user: UserCreate) -> models.User:
    db_user = models.User(
        login=user.login,
        password_hash=user.password,
        name=user.name,
        lastname=user.lastname,
        age=user.age,
        avatar=user.avatar,
        city=user.city,
        country=user.country,
    )
    try:
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user
    except sqlalchemy.exc.IntegrityError as e:
        raise DbError(str(e))


def get_all_countries(db: Session):
    return db.query(models.Countries).all()


def get_all_articles(db: Session, limit: int, offset: int) -> list[dict]:
    results = db.query(
        models.Article,
        models.User
    ).outerjoin(
        models.User, models.User.id == models.Article.author_id
    ).order_by(models.Article.created_at).offset(offset).limit(limit).all()

    joined_data = []
    for article, user in results:
        if user:
            joined_data.append({**article.__dict__, 'author': user.__dict__})
        else:
            joined_data.append({**article.__dict__, 'author': None})

    return joined_data


def get_article_by_id(db: Session, article_id: int) -> dict:
    db_article = db.query(
        models.Article,
        models.User,
    ).join(
        models.User, models.User.id == models.Article.author_id
    ).filter(
        models.Article.id == article_id
    ).first()

    if not db_article:
        raise NotFoundInDB(f'Статьи с id = {article_id} не существует')

    article, author = db_article

    blocks = db.query(models.Block).filter(
        models.Block.article_id == article.id
    ).all()

    article_data = {**article.__dict__, "author": author.__dict__, "blocks": []}
    for block in blocks:
        block_data = {**block.__dict__, "paragraphs": []}
        for paragraph in block.paragraphs:
            block_data["paragraphs"].append(paragraph.__dict__)
        article_data["blocks"].append(block)
    return article_data


def create_article(db: Session, article: ArticleCreate, author_id: int):
    with db.begin():
        db_article = models.Article(
                title=article.title,
                subtitle=article.subtitle,
                img=article.img,
                author_id=author_id,
                type=article.type,
            )
        db.add(db_article)
        db.flush()
        for block in article.blocks:
            db_block = models.Block(
                type=block.type,
                title=block.title,
                article_id=db_article.id,
            )
            db.add(db_block)
            db.flush()
            for paragraph in block.paragraphs:
                db_paragraph = models.Paragraph(
                    text=paragraph,
                    block_id=db_block.id,
                )
                db.add(db_paragraph)
        db.commit()
        return db_article


def update_article(db: Session, article: ArticleUpdate, current_user_id: int):
    db_article = db.query(models.Article).filter(models.Article.id == article.id)

    db_article_data = db_article.first()
    if not db_article_data:
        raise NotFoundInDB(f"Статьи с id = {article.id} не существует")

    if db_article_data.author_id != current_user_id:
        raise NoPermissionError(f"Статья с id = {article.id} написана другим пользователем")

    new_article = article.model_dump(exclude_unset=True, exclude_none=True, exclude={"id", "blocks"})

    for block in article.blocks:
        db_block = db.query(models.Block).filter(
            ((models.Block.id == block.id) & (models.Block.article_id == article.id)),
        )
        if not db_block.first():
            raise NotFoundInDB(f"Блок с id = {block.id} не найден")

        for paragraph in block.paragraphs:
            db_paragraph = db.query(models.Paragraph).filter(
                ((models.Paragraph.id == paragraph.id) & (models.Paragraph.block_id == block.id))
            )
            if not db_paragraph.first():
                raise NotFoundInDB(f"Параграф с id = {paragraph.id} не найден")
            db_paragraph.update(paragraph.model_dump(exclude_unset=True, exclude_none=True, exclude={"id"}))
        db_block.update(block.model_dump(exclude_unset=True, exclude_none=True, exclude={"id", "paragraphs"}))

    if new_article:
        db_article.update(new_article)

    db.commit()


def delete_article(db: Session, article_id: int, user_id: int):
    db_article = db.query(models.Article).filter_by(id=article_id)
    if db_article.count() == 0:
        raise NotFoundInDB(f"Статьи с id = {article_id} не существует")
    if db_article.first().author_id != user_id:
        if user_id != 1:
            raise NoPermissionError(f"Статья с id = {article_id} создана другим пользователем")
    db_blocks = db.query(models.Block).filter_by(article_id=article_id)
    block = db_blocks.first()
    db_paragraphs = db.query(models.Paragraph).filter_by(block_id=block.id)
    db_paragraphs.delete()
    db_blocks.delete()
    db_article.delete()
    db.commit()


def send_invite(db: Session, user_id: int, friend_id: int, message: str = None):
    result = db.query(models.Friend).filter(
        ((models.Friend.user_id == user_id) & (models.Friend.friend_id == friend_id)) |
        ((models.Friend.user_id == friend_id) & (models.Friend.friend_id == user_id)),
    ).all()

    if user_id == friend_id:
        raise AlreadyExists(f'Пользователь {user_id} не может дружить сам с собой')

    if result:
        raise AlreadyExists(f'Пользователь {user_id} уже дружит с пользователем {friend_id}, либо отправил заявку')

    db_friend = models.Friend(
        user_id=user_id,
        friend_id=friend_id,
        pending=True,
        message=message,
    )
    try:
        db.add(db_friend)
        db.commit()
    except sqlalchemy.exc.IntegrityError:
        raise NotFoundInDB(f'Пользователя с id={friend_id} не существует')


def get_friends(db: Session, user_id: int) -> dict:
    result = db.query(
        models.User, models.Friend
    ).join(
        models.Friend, ((models.User.id == models.Friend.user_id) | (models.User.id == models.Friend.friend_id))
    ).filter(
        (models.User.id != user_id),
        ((models.Friend.user_id == user_id) | (models.Friend.friend_id == user_id))
    ).distinct().all()

    friends = {
        "actual_friends": [],
        "sent_invites": [],
        "received_invites": [],
    }
    for user, friend in result:
        if friend.pending:
            if friend.user_id == user_id:
                friends["sent_invites"].append({"user": user, "info": friend})
            else:
                friends["received_invites"].append({"user": user, "info": friend})
        else:
            friends["actual_friends"].append({"user": user, "info": friend})
    return friends


def react_to_invite(db: Session, user_id: int, accept: bool, friend_id: int):
    friend = db.query(models.Friend).filter(
        or_(
            and_(
                models.Friend.friend_id == friend_id,
                models.Friend.user_id == user_id,
            ),
            and_(
                models.Friend.friend_id == user_id,
                models.Friend.user_id == friend_id
            )
        ),
    )

    if friend.count() == 0:
        raise NotFoundInDB(f"Запроса на дружбу пользователя {user_id} с пользователем {friend_id} не существует")

    db_friend = friend.first()

    # Если с заявкой работает автор заявки
    if db_friend.user_id == user_id:
        if accept:
            raise NoPermissionError(f"Пользователь {user_id} не может принять отправленную им заявку")

    if not db_friend.pending:
        raise NoPermissionError(f"Пользователь {user_id} уже принял запрос на дружбу с пользователем {friend_id}")

    if accept:
        db_friend.pending = False
        db_friend.message = None
    else:
        friend.delete()
    db.commit()


def delete_friend(db: Session, user_id: int, friend_id: int):
    friend = db.query(models.Friend).filter(
        ((models.Friend.user_id == user_id) & (models.Friend.friend_id == friend_id)) |
        ((models.Friend.user_id == friend_id) & (models.Friend.friend_id == user_id)),
        (models.Friend.pending == false())
    )

    if not friend.first():
        raise NotFoundInDB(f"Пользователь {user_id} не дружит с пользователем {friend_id}")

    friend.delete()
    db.commit()
