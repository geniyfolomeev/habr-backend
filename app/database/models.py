from datetime import datetime
from sqlalchemy import (
    Column,
    Boolean,
    Integer,
    String,
    ForeignKey,
    TIMESTAMP,
)
from sqlalchemy.orm import relationship
from app.database.engine import Base


class Countries(Base):
    __tablename__ = "countries"

    name = Column(String, nullable=False, primary_key=True)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    login = Column(String, nullable=False, unique=True)
    password_hash = Column(String, nullable=False)
    name = Column(String, nullable=False)
    lastname = Column(String, nullable=False)
    age = Column(Integer, nullable=True)
    avatar = Column(String, nullable=True)
    city = Column(String, nullable=True)
    country = Column(String, ForeignKey("countries.name"), nullable=True)


class Article(Base):
    __tablename__ = "articles"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    title = Column(String, nullable=False)
    subtitle = Column(String, nullable=False)
    img = Column(String, nullable=False)
    created_at = Column(TIMESTAMP, default=datetime.utcnow)
    author_id = Column(Integer, nullable=False)
    type = Column(String, nullable=False)


class Block(Base):
    __tablename__ = "blocks"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    type = Column(String, nullable=False)
    title = Column(String, nullable=True)
    article_id = Column(Integer, ForeignKey("articles.id"), nullable=False)

    paragraphs = relationship("Paragraph", back_populates="block")


class Paragraph(Base):
    __tablename__ = "paragraphs"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    text = Column(String, nullable=True)
    block_id = Column(Integer, ForeignKey("blocks.id"), nullable=False)

    block = relationship("Block", back_populates="paragraphs")


class Friend(Base):
    __tablename__ = "friends"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    friend_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    pending = Column(Boolean, nullable=False)
    message = Column(String, nullable=True)
