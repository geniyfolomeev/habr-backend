from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
)
from app.dependencies import (
    db,
    verify_token_access,
)
from app.database import (
    crud,
)
from app.database.crud import (
    NotFoundInDB,
    NoPermissionError,
)
from app.routers.models import (
    ArticleCreate,
    ArticleUpdate,
    ArticleCreatedResponse,
    ArticleLiteGetResponse,
    ArticleFullGetResponse,
)
from typing import Annotated
from sqlalchemy.orm import Session


router = APIRouter(
    tags=["Articles"]
)


@router.get("/articles", response_model=list[ArticleLiteGetResponse])
def get_articles(
        limit: int = 10,
        offset: int = 0,
        database: Session = Depends(db),
):
    if limit <= 0:
        raise HTTPException(
            status_code=400,
            detail="Incorrect limit"
        )
    if offset < 0:
        raise HTTPException(
            status_code=400,
            detail="Incorrect offset"
        )
    return crud.get_all_articles(database, limit, offset)


@router.get("/articles/{id}", response_model=ArticleFullGetResponse)
def get_article_by_id(
        id: int,
        database: Session = Depends(db),
):
    try:
        return crud.get_article_by_id(database, id)
    except NotFoundInDB as e:
        raise HTTPException(
            status_code=404,
            detail=str(e),
        )


@router.post("/articles/new", status_code=201, response_model=ArticleCreatedResponse)
def create_article(
        token_data: Annotated[dict, Depends(verify_token_access)],
        article: ArticleCreate,
        database: Session = Depends(db),
):
    db_article = crud.create_article(database, article, token_data['user_id'])
    return db_article


@router.put("/articles")
def update_article(
        token_data: Annotated[dict, Depends(verify_token_access)],
        article: ArticleUpdate,
        database: Session = Depends(db),
):
    try:
        return crud.update_article(database, article, token_data['user_id'])
    except NotFoundInDB as e:
        raise HTTPException(
            status_code=404,
            detail=str(e)
        )
    except NoPermissionError as e:
        raise HTTPException(
            status_code=403,
            detail=str(e)
        )


@router.delete("/articles/{id}")
def delete_article(
        id: int,
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    try:
        return crud.delete_article(database, id, token_data['user_id'])
    except NotFoundInDB as e:
        raise HTTPException(
            status_code=404,
            detail=str(e),
        )
    except NoPermissionError as e:
        raise HTTPException(
            status_code=403,
            detail=str(e),
        )
