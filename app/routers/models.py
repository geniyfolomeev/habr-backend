from pydantic import (
    BaseModel,
    Extra,
    Field,
)
from typing import (
    Optional,
    Literal,
)
from datetime import datetime


class Token(BaseModel):
    access_token: str


class UserCreate(BaseModel):
    login: str
    password: str
    name: str
    lastname: str
    age: int
    avatar: Optional[str] = None
    city: Optional[str] = None
    country: Optional[str] = None

    class Config:
        orm_mode = True
        extra = Extra.forbid


class UserRead(BaseModel):
    id: int
    login: str
    name: str
    lastname: str
    age: int
    avatar: Optional[str] = None
    city: Optional[str] = None
    country: Optional[str] = None

    class Config:
        orm_mode = True


class UserWithFriendShip(UserRead):
    is_friend: Literal["FRIEND", "YOU_REQUESTED_FRIENDSHIP", "REQUESTED_FRIENDSHIP_WITH_YOU", "NOT_FRIEND"]

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    id: int
    login: Optional[str] = None
    name: Optional[str] = None
    lastname: Optional[str] = None
    age: Optional[int] = None
    avatar: Optional[str] = None
    city: Optional[str] = None
    country: Optional[str] = None

    class Config:
        orm_mode = True
        extra = Extra.forbid


class Country(BaseModel):
    name: str


class BlockCreate(BaseModel):
    type: Literal["TEXT", "CODE"]
    title: Optional[str]
    paragraphs: list[str]


class Paragraph(BaseModel):
    id: int
    text: str


class BlockReadUpdate(BaseModel):
    id: int
    type: Literal["TEXT", "CODE"]
    title: Optional[str]
    paragraphs: Optional[list[Paragraph]]


class ArticleCreate(BaseModel):
    title: str
    subtitle: str
    img: str
    type: Literal["IT", "POLITICS", "RUSSIA/UKRAINE", "MOOD"]
    blocks: list[BlockCreate]

    class Config:
        extra = Extra.forbid


class ArticleUpdate(BaseModel):
    id: int
    title: Optional[str] = None
    subtitle: Optional[str] = None
    img: Optional[str] = None
    type: Optional[Literal["IT", "POLITICS", "RUSSIA/UKRAINE", "MOOD"]]
    blocks: Optional[list[BlockReadUpdate]]

    class Config:
        extra = Extra.forbid


class ArticleCreatedResponse(BaseModel):
    id: int


class ArticleLiteGetResponse(BaseModel):
    id: int
    title: str
    subtitle: str
    img: str
    created_at: datetime
    type: Literal["IT", "POLITICS", "RUSSIA/UKRAINE", "MOOD"]
    author_id: int
    author: Optional[UserRead] = None

    class Config:
        orm_mode = True


class ArticleFullGetResponse(ArticleLiteGetResponse):
    blocks: list[BlockReadUpdate]


class InviteRequest(BaseModel):
    friend_id: int
    message: Optional[str] = None

    class Config:
        extra = Extra.forbid


class FriendInfo(BaseModel):
    id: int
    user_id: int
    friend_id: int
    pending: bool
    message: Optional[str] = None

    class Config:
        orm_mode = True


class UserFriend(BaseModel):
    user: UserRead
    info: FriendInfo


class GetFriendsResponse(BaseModel):
    actual_friends: list[UserFriend]
    sent_invites: list[UserFriend]
    received_invites: list[UserFriend]


class ReactToInviteRequest(BaseModel):
    id: int
    accept: bool

    class Config:
        extra = Extra.forbid
