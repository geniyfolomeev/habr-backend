from fastapi import (
    APIRouter,
    Depends,
)
from app.dependencies import (
    db,
)
from app.database import (
    crud,
)
from app.routers.models import (
    Country,
)
from sqlalchemy.orm import Session


router = APIRouter(
    tags=["Countries"]
)

salt = b'$2b$12$20IvSRleZ7GUdGcusgecBu'


@router.get("/countries", response_model=list[Country])
def get_all_countries(
        database: Session = Depends(db),
):
    return crud.get_all_countries(database)
