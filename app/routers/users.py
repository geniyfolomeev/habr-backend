import bcrypt
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
)
from app.dependencies import (
    db,
    verify_token_access,
    create_access_token,
)
from app.database import (
    crud,
)
from app.database.crud import (
    NotFoundInDB,
    LoginIsNotUniq,
    DbError,
    NoPermissionError,
)
from sqlalchemy.orm import Session
from app.routers.models import (
    UserWithFriendShip,
    UserRead,
    UserCreate,
    UserUpdate,
    Token,
)
from typing import Annotated
from fastapi.security import OAuth2PasswordRequestForm


router = APIRouter(
    tags=["Users"]
)

salt = b'$2b$12$20IvSRleZ7GUdGcusgecBu'


def validate_request(user: UserCreate):
    if 0 <= len(user.login) < 5:
        raise HTTPException(
            status_code=400,
            detail="Login is too short"
        )
    if 0 <= len(user.password) <= 5:
        raise HTTPException(
            status_code=400,
            detail="Password is too short"
        )


@router.get("/users", response_model=list[UserWithFriendShip])
def get_all_users(
        token_data: Annotated[dict, Depends(verify_token_access)],
        login: str = None,
        name: str = None,
        lastname: str = None,
        limit: int = 10,
        offset: int = 0,
        database: Session = Depends(db),
):
    if limit <= 0:
        raise HTTPException(
            status_code=400,
            detail="Incorrect limit"
        )
    if offset < 0:
        raise HTTPException(
            status_code=400,
            detail="Incorrect offset"
        )
    return crud.get_users_filter(
        db=database,
        user_id=token_data["user_id"],
        login=login,
        name=name,
        lastname=lastname,
        limit=limit,
        offset=offset,
    )


@router.get("/users/me", response_model=UserRead)
def get_current_user(
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    return crud.get_user_by_id(database, token_data['user_id'])


@router.get("/users/{id}", response_model=UserRead)
def get_user_by_id(
        id: int,
        database: Session = Depends(db),
):
    try:
        return crud.get_user_by_id(database, id)
    except NotFoundInDB as e:
        raise HTTPException(
            status_code=404,
            detail=str(e),
        )


@router.delete("/users/{id}")
def delete_user(
        id: int,
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    try:
        return crud.delete_user(database, id, token_data['user_id'])
    except NotFoundInDB as e:
        raise HTTPException(
            status_code=404,
            detail=str(e),
        )
    except NoPermissionError as e:
        raise HTTPException(
            status_code=403,
            detail=str(e),
        )


@router.put("/users", dependencies=[Depends(verify_token_access)], response_model=UserRead)
def update_user(
        user: UserUpdate,
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    if user.login:
        if 0 <= len(user.login) < 5:
            raise HTTPException(
                status_code=400,
                detail="Login is too short"
            )
    try:
        return crud.update_user(database, user, token_data['user_id'])
    except (LoginIsNotUniq, NotFoundInDB, DbError) as e:
        raise HTTPException(
            status_code=400,
            detail=str(e)
        )
    except NoPermissionError as e:
        raise HTTPException(
            status_code=403,
            detail=str(e)
        )


@router.post("/users/new", response_model=Token)
def create_user(
        user: UserCreate,
        database: Session = Depends(db),
):
    validate_request(user)
    if crud.get_user_by_login(database, user.login):
        raise HTTPException(
            status_code=400,
            detail="Login is not uniq"
        )
    user.password = bcrypt.hashpw(bytes(user.password, encoding='utf8'), salt).decode('utf8')
    try:
        user_db = crud.create_new_user(database, user)
    except DbError:
        raise HTTPException(
            status_code=400,
            detail=f"Country = {user.country} is not a valid country. Look for GET: /countries"
        )
    return Token(access_token=create_access_token(user_db.id))


@router.post("/users/login", response_model=Token)
def login(
        form_data: OAuth2PasswordRequestForm = Depends(),
        database: Session = Depends(db),
):
    if 0 <= len(form_data.username) < 5:
        raise HTTPException(
            status_code=401,
        )
    if 0 <= len(form_data.password) <= 5:
        raise HTTPException(
            status_code=401,
        )
    user_db = crud.get_user_by_login(database, form_data.username)
    if not user_db:
        raise HTTPException(
            status_code=401,
        )

    if user_db.password_hash != bcrypt.hashpw(bytes(form_data.password, encoding='utf8'), salt).decode('utf8'):
        raise HTTPException(
            status_code=401,
        )
    return Token(access_token=create_access_token(user_db.id))
