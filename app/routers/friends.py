from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
)
from app.dependencies import (
    db,
    verify_token_access,
)
from app.database import (
    crud,
)
from app.database.crud import (
    AlreadyExists,
    NotFoundInDB,
    NoPermissionError,
)
from app.routers.models import (
    InviteRequest,
    GetFriendsResponse,
    ReactToInviteRequest,
)
from typing import Annotated
from sqlalchemy.orm import Session


router = APIRouter(
    tags=["Friends"]
)


@router.get("/friends", response_model=GetFriendsResponse)
def get_friends(
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    return crud.get_friends(database, token_data['user_id'])


@router.post("/friends", status_code=201)
def send_invite(
        invite: InviteRequest,
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    try:
        return crud.send_invite(database, token_data['user_id'], invite.friend_id, invite.message)
    except (AlreadyExists, NotFoundInDB) as e:
        raise HTTPException(
            status_code=400,
            detail=str(e)
        )


@router.put("/friends")
def react_to_invite(
        reaction: ReactToInviteRequest,
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    try:
        return crud.react_to_invite(database, token_data["user_id"], reaction.accept, reaction.id)
    except (NoPermissionError, NotFoundInDB) as e:
        raise HTTPException(
            status_code=400,
            detail=str(e),
        )


@router.delete("/friends/{id}")
def delete_friend(
        id: int,
        token_data: Annotated[dict, Depends(verify_token_access)],
        database: Session = Depends(db),
):
    try:
        return crud.delete_friend(database, token_data["user_id"], id)
    except NotFoundInDB as e:
        raise HTTPException(
            status_code=400,
            detail=str(e)
        )
